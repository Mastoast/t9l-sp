local Gamestate = require 'modules.hump.gamestate'

Credits = {}

function Credits:init()
end

function Credits:enter(oldState)
    self.oldState = oldState
end

function Credits:update(dt)
end

function Credits:draw()
    love.graphics.print('Programming by Mastoast',5,10)
    love.graphics.print('Background and logo design by Setsuretsu',5,35)
end

function Credits:keypressed(key)
    Gamestate.switch(self.oldState)
end

return Credits