local Gamestate = require 'modules.hump.gamestate'
local Lobby

Result = {}

function Result:init()
end

function Result:enter(old_state, return_state)
    self.return_state = return_state
    self.score = old_state.score
    self.nbPlayers = old_state.nbPlayers or nil
    self.nbPlayersAlive = old_state.nbPlayersAlive or nil
    self.attacker = old_state.lastAttacker or nil
    self.nbFrags = old_state.nbFrags or nil
    Lobby = old_state.lobby_state
    love.graphics.setColor(1, 1, 1)
end

function Result:update(dt)
end

function Result:draw()
    if self.nbPlayersAlive then
        if tonumber(self.nbPlayersAlive) == 1 then
            -- victory
            love.graphics.print('TOP 1 yeah !', 50, 50)
        elseif self.attacker then
            -- last attacker
            love.graphics.print(string.format("Killed by %s",self.attacker), 350, 450)
            love.graphics.print('Game Over', 50, 50)
        end
        -- position
        love.graphics.print(string.format('Position : %i / %i', self.nbPlayersAlive, self.nbPlayers), 350, 400)
    else
        love.graphics.print('Game Over', 50, 50)
    end
    -- score
    love.graphics.print(string.format('Score : %i', self.score), 350, 350)

    -- number of frags
    if self.nbFrags then
        love.graphics.print(string.format("Frags: %i",self.nbFrags), 350, 500)
    end
end

function Result:keypressed(key)
    if key == 'escape' or key == 'return' then
        Gamestate.switch(self.return_state)
    end
end

return Result
