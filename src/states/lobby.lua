local Gamestate = require 'modules.hump.gamestate'
local Game = require 'src.gamemodes.game_br'

local socket = require("socket")

Lobby = {}

function Lobby:init()
end

function Lobby:enter(oldState, ip, port, name)
    -- UDP
    self.udp = socket.udp()
    self.udp:settimeout(0.1)
    self.udp:setpeername(ip, port)
    self.udpQueryTimer = 0
    self.udpQueryDelay = 0.2
    --
    self.name = name
    self.client_id = oldState.client_id
    self.status = 'NOT READY'
    self.nbPlayers = 0
    self.playersList = {}
    --
    self.oldState = oldState
end

function Lobby:update(dt)
    self.udpQueryTimer = self.udpQueryTimer + dt
    if self.udpQueryTimer > self.udpQueryDelay then
        self.udpQueryTimer = 0
        -- get nb players
        self.udp:send(string.format("%s PLAYERS", self.client_id))
        self:receiveUdp()
        -- get server status
        self.udp:send(string.format("%s IS_STARTED", self.client_id))
        self:receiveUdp()
    end
end

function Lobby:receiveUdp()
    local server_data, status, partial = self.udp:receive()
    if server_data then
        if(server_data == "STARTED") then
            Gamestate.switch(Game, self.udp)
        else
            message, params = server_data:match("^(%S*) (.*)")
            if(message == "PLAYERS") then
                self.playersList = {}
                local idSeparator = string.find( params, ';' )
                while idSeparator do
                    local onePlayer = params:sub(1, idSeparator)
                    params = params:sub(idSeparator + 1)
                    pName, pRdy = onePlayer:match("^(%S*)/(%d);")
                    table.insert(self.playersList, {pName,pRdy})
                    idSeparator = string.find( params, ';' )
                end
                self.nbPlayers = #self.playersList
            end
        end
    end
end

function Lobby:draw()
    --title
    love.graphics.print('Tetrominos Fury 2351', 50, 50)
    -- print players
    for i, player in ipairs(self.playersList) do
        love.graphics.print(player[1], 800, i*50)
        if player[2] == '1' then
            love.graphics.print("READY", 100 + 800, i*50)
        else
            love.graphics.print("NOT READY", 100 + 800, i*50)
        end
    end
    -- status
    love.graphics.print(string.format( "Players in the lobby : %s",self.nbPlayers ), 300,300)
    love.graphics.print(self.status, 550, 550)
end

function Lobby:keypressed(key)
    if key == 'escape' then
        self.udp:send(string.format( "%s DISCONNECT", self.client_id ))
        Gamestate.switch(self.oldState)
    elseif key == 'return' or key == 'kpenter' then
        self:changeStatus()
    end
end

function Lobby:changeStatus()
    if self.status == 'READY' then
        self.status = 'NOT_READY'
    else
        self.status = 'READY'
    end
    self.udp:send(string.format( "%s %s", self.client_id, self.status ))
end

return Lobby
