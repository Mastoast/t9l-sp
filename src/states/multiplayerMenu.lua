local Gamestate = require 'modules.hump.gamestate'
local Lobby = require 'src.states.lobby'
local socket = require("socket")

local TextField = require 'src.objects.textfield'

local triangleSprite = love.graphics.newImage('res/triangle.png')

local fields = {'name', 'ip', 'port'}
MultiplayerMenu = {}

function MultiplayerMenu:init()
end

function MultiplayerMenu:enter(oldState)
    -- Connection parameters
    self.name = TextField(100, 220, 400, 100, savedName or 'Player', 'Name')
    self.port = TextField(650, 400, 200, 100, savedPort or '12345', 'PORT')
    self.ip = TextField(100, 400, 400, 100, savedIp or '127.0.0.1', 'Host IP')
    self.focus = 1
    self.client_id = 'none'
    self.status = ''
    self.charLimit = 20
    --
    self.triangleSize = 15
	self.spriteScale = 1 / triangleSprite:getWidth() * self.triangleSize
end

function MultiplayerMenu:update(dt)
end

function MultiplayerMenu:draw()
    love.graphics.print('Tetrominos Fury 2351', 550, 50)
    -- Text fields
    self.ip:draw()
    self.port:draw()
    self.name:draw()
    -- draw focus arrow
    if self.focus then
        love.graphics.draw(triangleSprite, self[fields[self.focus]].x - self.triangleSize*2, self[fields[self.focus]].y - self.triangleSize/2, 0, self.spriteScale, self.spriteScale)
    end
    -- status messages
    love.graphics.print(self.status, 550, 550)
    --
    love.graphics.print('Press ENTER to connect', 700,700)
end

function MultiplayerMenu:keypressed(key)
    -- Interact with text field
    if self.focus then
        self[fields[self.focus]]:keypressed(key)
    end
    -- Set the focus
    if (key == 'right') or (key == 'down') then
        self.focus = self.focus + 1
        if self.focus > #fields then
            self.focus = 1
        end
    elseif (key == 'left') or (key == 'up') then
        self.focus = self.focus - 1
        if self.focus < 1 then
            self.focus = #fields
        end
    end
    -- Try connecting
    if key == 'return' or key == 'kpenter' then
        savedIp = self.ip.text
        savedPort = self.port.text
        savedName = self.name.text
        self:tryConnecting()
    elseif key == 'escape' then
        Gamestate.switch(menuState)
    end
end

function MultiplayerMenu:tryConnecting()
    self.udp = socket.udp()
    self.udp:settimeout(1)
    if self.udp:setpeername(self.ip.text, self.port.text) ~= nil then
        -- Ask infos
        self.udp:send(string.format("%s CONNECT %s", self.client_id, self.name.text));
        local server_data, status, partial = self.udp:receive()
        if server_data then
            -- Server answer
            if server_data == "BUSY" then
                self.status = 'Server is busy. Try again later'
            else
                self.client_id = server_data:match("^CONNECTED (%S*)")
                Gamestate.switch(Lobby, self.ip.text, self.port.text, self.name.text)
            end
        elseif( status == 'timeout' ) then
            self.status = 'Connection timed out'
        end
    else
        self.status = 'Wrong syntax'
    end
end

return MultiplayerMenu
