local Gamestate = require 'modules.hump.gamestate'

local SoloGame = require 'src.gamemodes.game'
local multiplayerMenu = require 'src.states.multiplayerMenu'
local creditsMenu = require 'src.states.credits'
local Title = require 'src.objects.title'

local triangleSprite = love.graphics.newImage('res/triangle.png')
local backgroundSprite = love.graphics.newImage('res/tf2351bg.png')

Menu = {}

function Menu:init()
    -- Init scene
    self.state = 'wait'
    self.title = Title()
    -- Get version
    file = love.filesystem.newFile("VERSION")
    file:open("r")
    self.version = file:read()
    file:close()
    -- Init options in the main menu
    self.options = {}
    table.insert( self.options, { name = 'SINGLEPLAYER', state = SoloGame } )
    table.insert( self.options, { name = 'MULTIPLAYER', state = multiplayerMenu } )
    table.insert( self.options, { name = 'CREDITS', state = creditsMenu } )
    table.insert( self.options, { name = 'EXIT GAME' } )
    self.selected = 1
    --
    self.triangleSize = 15
    self.spriteScale = 1 / triangleSprite:getWidth() * self.triangleSize
    self.backgroundScaleX = 1 / backgroundSprite:getWidth() * screen.x
    self.backgroundScaleY = 1 / backgroundSprite:getHeight() * screen.y
end

function Menu:enter(oldstate)
end

function Menu:update(dt)
    self.title:update(dt)
end

function Menu:draw()
    -- Background
    love.graphics.draw(backgroundSprite, 0, 0, 0, self.backgroundScaleX, self.backgroundScaleY)
    -- Title
    self.title:draw()
    -- Version
    love.graphics.print(self.version, 10, screen.y-20)
    -- Press any key
    if self.state == 'wait' then
        love.graphics.print('PRESS ANY KEY', screen.x/2 - 45, screen.y/2 + 150)
    -- Actual menu
    else
        -- Options
        for i, option in ipairs(self.options) do
            love.graphics.print(option.name, screen.x/2 - 40, screen.y/2 + i*40)
        end
        -- Selection cursor
        love.graphics.draw(triangleSprite, screen.x/2 - 65, screen.y/2 + self.selected*40 - self.triangleSize/2, 0, self.spriteScale, self.spriteScale)
    end
    
end

function Menu:keypressed(key)
    -- Actual Main Menu
    if(self.state == 'select') then
        if key == 'up' then
            -- cursor up
            if self.selected <= 1 then
                self.selected = #self.options
            else
                self.selected = self.selected - 1
            end
        elseif key == 'down' then
            -- cursor down
            if self.selected >= #self.options then
                self.selected = 1
            else
                self.selected = self.selected + 1
            end
        elseif key == 'return' then
            if self.options[self.selected].name ~= 'EXIT GAME' then
                Gamestate.switch(self.options[self.selected].state)
            else
                self.exit()
            end
        elseif key == 'escape' then
            self.state = 'wait'
        end
    -- Waiting input
    else
        if key == 'escape' then
            self.exit()
        end
        self.state = 'select'
    end
end

function Menu.exit()
    love.event.quit()
end

return Menu