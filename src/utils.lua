util = {}

function util.clone(origin)
	local clone = {}
	if type(origin) ~= 'table' then
		return origin
	end
	for k, v in pairs(origin) do
		clone[k] = util.clone(v)
	end
	return clone
end

function util.printGrid(table)
	print('-------------------------------')
	for i=1, #table do
		local strLine = i..' | '
		for j=1, #table[i] do
			if table[i] == nil then
				strLine = 'null'
				break
			else
				strLine = strLine..table[i][j]..' | '
			end
		end
		print(strLine)
	end
	print('-------------------------------')
end

function util.printTab(origin, indent)
	-- indenting
	local indent = indent or 0
	-- if value
	if type(origin) ~= 'table' then
		local text = ''
		for i=0, indent do
			text = text..'  '
		end
		text = text..tostring(origin)
		print(text)
	-- if table
	else
		for k, v in pairs(origin) do
			local text = ''
			for i=0, indent do
				text = text..'  '
			end
			text = text..k
			print(text)
			util.printTab(v,indent + 1)
		end
	end
end

function util.tablelength(T)
	local count = 0
	for _ in pairs(T) do count = count + 1 end
	return count
end

function util.tableKeys(T)
	keyList = {}
	for k, v in pairs(T) do
		table.insert(keyList, k)
	end
	return keyList
end

function util.randomElement(T)
	randId = math.random( util.tablelength(T) )
	return T[util.tableKeys(T)[randId]]
end

return util
