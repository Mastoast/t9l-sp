local Class = require 'modules.hump.class'

local titleSprite = love.graphics.newImage('res/tf2351titre.png')

Title = Class{}

function Title:init()
	self.x = screen.x/2 - 370
	self.y = 0
    self.scale = 0.4
    self.yMin = self.y - 20
    self.yMax = self.y + 20
    self.state = 'up'
    self.speed = 10
end

function Title:update(dt)
    -- Going up
    if self.state == 'up' then
        self.y = self.y + self.speed * dt
        -- Change direction
        if self.y >= self.yMax then
            self.state = 'down'
        end
    else
        self.y = self.y - self.speed * dt
        if self.y <= self.yMin then
            self.state = 'up'
        end
    end
end

function Title:draw()
    love.graphics.draw(titleSprite, self.x, self.y, 0, self.scale, self.scale)
end

return Title
