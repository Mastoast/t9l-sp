local Class = require 'modules.hump.class'

local squareSprite = love.graphics.newImage('res/square.png')

Grid = Class{}

function Grid:init(x,y)
	self.x = x
	self.y = y
	self.width = 10
	self.height = 21
	self.squareSize = 35
	self.spriteScale = 1 / squareSprite:getWidth() * self.squareSize
	-- preset a blank line
	self.blankLine = {}
	for i=1, self.width do
		self.blankLine[i] = 'blank'
	end
	-- preset a grey line
	self.greyLine = {}
	for i=1, self.width do
		self.greyLine[i] = 'grey'
	end
	-- init the grid
	self.grid = {}
	for i=1, self.height do
		self.grid[i] = utils.clone(self.blankLine)
	end
end

function Grid:draw(currentPiece)
	-- Draw tiles
	for i=2, self.height do
		for j=1, self.width do
			content = self.grid[i][j]
			if (content == 'blank') then
				love.graphics.setColor(0, 0, 0)
				love.graphics.rectangle('line', self.x + (j-1) * self.squareSize, self.y + (i-1) * self.squareSize,  self.squareSize, self.squareSize)
			else
				love.graphics.setColor(colors[content])
				love.graphics.draw(squareSprite, self.x + (j-1) * self.squareSize, self.y + (i-1) * self.squareSize,  0, self.spriteScale, self.spriteScale)
			end
		end
	end
	-- Draw current piece
	local blocks = currentPiece.blocks
	local posX = currentPiece.pos.x
	local posY = currentPiece.pos.y
	love.graphics.setColor(colors[currentPiece.color])
	for i=1, #blocks do
		for j=1,#blocks[i] do
			if blocks[i][j] == 1 then
				love.graphics.draw(squareSprite, self.x + (j-1 + posX-1) * self.squareSize, self.y + (i-1 + posY-1) * self.squareSize,  0, self.spriteScale, self.spriteScale)
			end
		end
	end
end

-- utilities
function Grid:checklines()
	-- check lines
	local completedLines = 0
	for i=1, self.height do
		if self.completed(self.grid[i]) then
			completedLines = completedLines + 1
			self.grid[i] = nil
		end
	end
	-- clean grid
	for i=1, self.height do
		if self.grid[i] == nil then
			table.remove( self.grid, i )
			table.insert( self.grid, 1, utils.clone(self.blankLine) )
			i = i - 1
		end
	end
	return completedLines
end

function Grid:canFit(piece)
	local blocks = piece.blocks
	local posX = piece.pos.x
	local posY = piece.pos.y
	local fit = true
	for i=1, #blocks do
		for j=1, #blocks[i] do
			if blocks[i][j] == 1 then
				-- Test grid limits
				if (posY + i - 1 > self.height) then
					fit = false
					break
				end
				if (posX + j - 1 > self.width) then
					fit = false
					break
				end
				if (posX + j - 1 <= 0) then
					fit = false
					break
				end
				-- Test collision
				if self.grid[posY + i - 1][(posX + j - 1)] ~= 'blank' then
					fit = false
					break
				end
			end
		end
	end
	return fit
end

function Grid:addPiece(piece)
	local blocks = piece.blocks
	local posX = piece.pos.x
	local posY = piece.pos.y
	local color = piece.color
	for i=1, #blocks do
		for j=1, #blocks[i] do
			if blocks[i][j] == 1 then
				self.grid[posY + i-1][posX + j-1] = color
			end
		end
	end
	-- check if line
	return self:checklines()
end

function Grid:addGreyLine()
	table.remove( self.grid, 1 )
	local greyLine = utils.clone(self.greyLine)
	greyLine[math.random( #greyLine )] = 'blank'
	table.insert( self.grid, greyLine )
end

function Grid.completed(line)
	local completed = true
	for j=1, #line do
		if line[j] == 'blank' then
			completed = false
			break
		end
	end
	return completed
end

return Grid
