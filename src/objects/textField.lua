local Class = require 'modules.hump.class'

TextField = Class{}

function TextField:init(x,y,width,height,text,title)
	self.x = x
	self.y = y
	self.width = width
    self.height = height
    self.text = text or ''
    self.title = title or ''
    self.charLimit = 20
end

function TextField:update()
end

function TextField:draw()
    love.graphics.print(self.title, self.x, self.y - 20)
    love.graphics.rectangle('line', self.x, self.y, self.width, self.height)
    love.graphics.print(self.text, self.x, self.y)
end

function TextField:keypressed(key)
    -- handle different input for dots
    if key == ':' or key == '.' or key == ';' or key == ',' or key == 'kp.' then
        self:write('.')
    -- Remove character
    elseif key == 'backspace' then
        self:remove(text)
    -- Copy clipboard
    elseif key == 'v' then
        if love.keyboard.isDown('rctrl') or love.keyboard.isDown('lctrl') then
            self:paste()
        end
    elseif key == 'c' then
        if love.keyboard.isDown('rctrl') or love.keyboard.isDown('lctrl') then
            self:copy()
        end
    end
    -- Write numbers
    for i=0, 9 do
        if tonumber(key) == i or tonumber(key:sub(3,3)) == i then
            self:write(tostring(i))
        end
    end
    -- Write letters
    for i=string.byte( 'a' ),string.byte( 'z' ) do
        if key == string.char(i) then
            if not (love.keyboard.isDown('lctrl') or love.keyboard.isDown('rctrl')) then
                if love.keyboard.isDown('rshift') or love.keyboard.isDown('lshift') then
                    self:write(string.upper( key ))
                else
                    self:write(key)
                end
            end
        end
    end
end

-- utilities
function TextField:paste()
    local text = love.system.getClipboardText():sub(1, self.charLimit)
    if #text >= 1 then
        local result = ''
        for i=1, #text do
            local isLetter = false
            local isNumber = false
            local isSpec = false
            if (string.byte( text:sub(i,i) ) >= 97) and (string.byte( text:sub(i,i) ) <= 122) then isLetter = true end
            if (string.byte( text:sub(i,i) ) >= 48 and string.byte( text:sub(i,i) ) <= 57) then isNumber = true end
            if text:sub(i,i) == '.' then isSpec = true end
            if isLetter or isNumber or isSpec then
                result = result..text:sub(i,i)
            end
        end
        self.text = result
    end
end

function TextField:copy()
    print(self.text)
    love.system.setClipboardText(self.text)
end

function TextField:write(newChar)
    if (newChar ~= nil) and (#self.text < self.charLimit) then
        self.text = self.text..newChar
    end
end

function TextField:remove()
    self.text = self.text:sub(1, -2)
end

return TextField
