local Gamestate = require 'modules.hump.gamestate'
local Class = require 'modules.hump.class'

local Game = require 'src.gamemodes.game'
local Result = require 'src.states.result'

local Tetrominos = require 'src.tetrominos'

local pieces = {'I','J','L','O','S','T','Z'}
local scores = { 40, 100, 300, 1200 }
local keys = {'left', 'right', 'down'}

local squareSprite = love.graphics.newImage('res/square.png')

BRGame = Class{}
BRGame:include(Game)

function BRGame:enter(old_state)
	-- Super:enter(old_state)
	Game.enter(self, old_state)
	self.udp = old_state.udp or nil
	if self.udp then
		self.lobby_state = old_state
		self.udpTimer = 2
		self.udpQueryDelay = 0.5
		self.request = 'status'
		self.lastMessage = 0
		self.udp:settimeout(0.05)
	end
	self.client_id = old_state.client_id
	self.nbFrags = 0
	self.lastAttacker = nil
	-- initiate player lists
	self.playersList = old_state.playersList
	for id, player in ipairs(self.playersList) do
		player[2] = '1'
	end
	self.nbPlayers = #self.playersList
	self.nbPlayersAlive = #self.playersList
end

function BRGame:update(dt)
	self:updateAutoShift(dt)
	self:updateLock(dt)
	self:updateFall(dt)
	self:updateServerStatus(dt)
end

function BRGame:keypressed(key)
	if key == 'escape' then
		self.udp:send(string.format("%s GAME_OVER", self.client_id))
		Gamestate.switch(Result, menuState)
	end
	-- Player controls
	self:reactToKey(key)
end

function BRGame:checkGameOver()
	local updatedPiece = utils.clone(self.currentPiece)
	if not self.grid:canFit(updatedPiece) then
		self.udp:send(string.format("%s GAME_OVER", self.client_id))
		Gamestate.switch(Result, menuState)
	end
end

function BRGame:addScore(nbRemovedLines)
	if nbRemovedLines < 5 then
		if nbRemovedLines ~= 0 then
			self.score = self.score + (1/self.fallFrequency)*scores[nbRemovedLines]
			self:sendUdp(string.format("%s LINES_COMPLETED %i", self.client_id, nbRemovedLines))
		end
	end
end

-- Draw Score
function BRGame:drawScore()
	Game.drawScore(self)
	love.graphics.print(string.format('POSITION : %i/%i', self.nbPlayersAlive, self.nbPlayers), 0, 30)
	love.graphics.print(string.format('FRAGS : %i', self.nbFrags), 0, 60)
	-- draw player list
	for i, player in pairs(self.playersList) do
		local status
		if player[2] == '1' then
			status = 'ALIVE'
		else
			status = 'DEAD'
		end
		love.graphics.print(string.format('%s - %s', player[1], status), 20, 120 + 10*i)
	end
end

-- check server infos
function BRGame:updateServerStatus(dt)
	self.lastMessage = self.lastMessage + dt
	self.udpTimer = self.udpTimer + dt
	if self.udpTimer >= self.udpQueryDelay then
		self.udpTimer = 0
		if self.request == 'status' then
			-- refresh player number
			self.udp:send(string.format("%s GET_STATUS", self.client_id))
			self:receiveUdp()
			self.request = 'lines'
		else
			-- check new lines
			self.udp:send(string.format("%s NEW_LINES", self.client_id))
			self:receiveUdp()
			self.request = 'status'
		end
	end
end

function BRGame:receiveUdp()
	local server_data, status, partial = self.udp:receive()
	if server_data then
		self.lastMessage = 0
		message, params, attacker = server_data:match("^(%S*) (%d) (%S*)")
		if(message == "NB_LINES") then
			self:addGreyLines(tonumber(params))
			self.lastAttacker = attacker
		else
			self:getStatus(server_data)
		end
    end
end

function BRGame:getStatus(data)
	message, playersStatus, nbFrags, fallFrequency = data:match("^(%S*) (%S*) (%d) (.+)")
	if (message == "STATUS") then
		local nbAlive = 0
		for i = 1, #playersStatus do
			local status = playersStatus:sub(i,i)
			self.playersList[i][2] = status
			if (status == "1") then 
				nbAlive = nbAlive + 1
			end
		end

		self.nbPlayersAlive = nbAlive
		self.nbFrags = nbFrags
		self.fallFrequency = tonumber(fallFrequency)
		if (tonumber(self.nbPlayersAlive) == 1 and (self.nbPlayers ~= 1)) then
			self.udp:send(string.format("%s WINNER", self.client_id))
			Gamestate.switch(Result, menuState)
		end
	end
end

function BRGame:sendUdp(msg)
	self.udp:send(msg)
	local server_data, status, partial = self.udp:receive()
	if server_data then
		self.lastMessage = 0
		message, params = server_data:match("^(%S*) (.*)")
		return message, params
	end
end

return BRGame
