local Gamestate = require 'modules.hump.gamestate'
local Class = require 'modules.hump.class'

local Grid = require 'src.objects.grid'
local Result = require 'src.states.result'

local Tetrominos = require 'src.tetrominos'

local pieces = {'I','J','L','O','S','T','Z'}
local scores = { 40, 100, 300, 1200 }
local keys = {'left', 'right', 'down'}

local squareSprite = love.graphics.newImage('res/square.png')

Game = Class{}

function Game:init()
end

function Game:enter(oldState)
	-- menu
	self.oldState = oldState
	-- difficulty
	self.totalTime = 0
	self.difficulty = 1
	self.intervalDifficulty = 20.0
	-- fall timers
	self.timer = 0
	self.fallFrequency = 0.8
	-- keys pressed
	self.keysPressed = {}
	self:initShifts()
	-- auto Shift
	self.autoShiftStartDelay = 0.2
	self.shifting = false
	self.autoShiftDelay = 0.05
	self.hasSwap = false
	self.hasLock = false
	-- time before locking a piece
	self.lockTimer = nil
	self.lockDelay = 0.5
	--time before spawning new piece
	self.spawnTimer = nil
	self.spawnDelay = 0.5
	-- score
	self.score = 0
	-- initialize the grid
	self.grid = Grid( 400, 50 )
	-- pieces queue
	self.currentPiece = {}
	self.savedPiece = nil
	self.nextPieces = {}
	repeat
		self:createNextRandomPiece()
	until ( #self.nextPieces == 5 )
	self:spawnNextPiece()
	-- graphism
	self.squareSize = 25
	self.spriteScale = 1 / squareSprite:getWidth() * self.squareSize
end

-- increasing difficulty over time
function Game:updateDifficulty(dt)
	self.totalTime = self.totalTime + dt
	if self.totalTime > (self.difficulty * self.intervalDifficulty) then
		self.difficulty = self.difficulty + 1
		if (self.fallFrequency > 0.3) then
			self.fallFrequency = self.fallFrequency - 0.1
		else
			if (self.fallFrequency > self.autoShiftDelay) then
				self.fallFrequency = self.fallFrequency - 0.01
			end
		end
	end
end

-- handle auto shift
function Game:updateAutoShift(dt)
	for key, values in pairs(self.keysPressed) do
		if (love.keyboard.isDown(key)) then
			self.keysPressed[key].autoShiftTimer = self.keysPressed[key].autoShiftTimer + dt
			if (self.keysPressed[key].shifting == false) then
				if (self.keysPressed[key].autoShiftTimer > self.autoShiftStartDelay) then
					self.keysPressed[key].shifting = true
					self.keysPressed[key].autoShiftTimer = 0
				end
			else
				if self.keysPressed[key].autoShiftTimer > self.autoShiftDelay then
					self.keysPressed[key].autoShiftTimer = 0
					self:reactToKey(key)
				end
			end
		end
	end
end

-- handle lock
function Game:updateLock(dt)
	if self.lockTimer then
		self.lockTimer = self.lockTimer + dt
		if self.lockTimer > self.lockDelay then
			local updatedPiece = utils.clone(self.currentPiece)
			updatedPiece.pos.y = updatedPiece.pos.y + 1
			if not self.grid:canFit(updatedPiece) then
				local nbRemovedLines = self.grid:addPiece(self.currentPiece)
				self.hasSwap = false
				self:addScore(nbRemovedLines)
				self:spawnNextPiece()
			end
			self.lockTimer = nil
			self.hasLock = false
		end
	end
end

-- handle piece fall
function Game:updateFall(dt)
	self.timer = self.timer + dt
	if self.timer >= self.fallFrequency then
		self.timer = 0
		local updatedPiece = utils.clone(self.currentPiece)
		updatedPiece.pos.y = updatedPiece.pos.y + 1
		if self.grid:canFit(updatedPiece) then
			self.currentPiece = updatedPiece
			self.lockTimer = nil
		else
			if not self.lockTimer then
				self.lockTimer = 0
				self.hasLock = true
			end
		end
	end
end

function Game:update(dt)
	self:updateDifficulty(dt)
	self:updateAutoShift(dt)
	self:updateFall(dt)
	self:updateLock(dt)
end

-- Draw Grid
function Game:drawGrid()
	self.grid:draw(self.currentPiece)
end

-- Draw Score
function Game:drawScore()
	love.graphics.setColor(1,1,1)
	love.graphics.print(string.format('SCORE : %i', self.score), 0, 0)
end

-- Draw next pieces
function Game:drawNextPieces()
	love.graphics.print("INCOMING PIECES", 815, 50)
	for i,piece in ipairs(self.nextPieces) do
		love.graphics.setColor(colors[piece.color])
		for k=1, #piece.blocks do
			for l=1, #piece.blocks[k] do
				if piece.blocks[k][l] == 1 then
					love.graphics.draw(squareSprite, 800 + self.squareSize*l, 800 + self.squareSize*k - 130*i, 0, self.spriteScale, self.spriteScale)
				end
			end
		end
	end
end

-- Draw saved piece
function Game:drawSavedPiece()
	if self.savedPiece then
		love.graphics.setColor(colors[self.savedPiece.color])
		for k=1, #self.savedPiece.blocks do
			for l=1, #self.savedPiece.blocks[k] do
				if self.savedPiece.blocks[k][l] == 1 then
					love.graphics.draw(squareSprite, 200 + self.squareSize*l, 100 + self.squareSize*k, 0, self.spriteScale, self.spriteScale)
				end
			end
		end
		love.graphics.setColor(1,1,1)
		love.graphics.print("SAVED PIECE", 225, 50)
	end
end

-- Draw difficulty
function Game:drawDifficulty()
	love.graphics.setColor(1,1,1)
	love.graphics.print(string.format("DIFFICULTY: %i", self.difficulty),400,0)
end

-- Draw latency
function Game:drawLatency()
	love.graphics.print(self.lastMessage, 0, 750)
end

function Game:draw()
	self:drawGrid()
	self:drawScore()
	self:drawNextPieces()
	self:drawSavedPiece()
	self:drawDifficulty()
end

function Game:keypressed(key)
	-- if key == 'f' then self:addGreyLines(1) end
	if key == 'escape' then
		Gamestate.switch(Result, self.oldState)
	end
	-- Player controls
	self:reactToKey(key)
end

function Game:reactToKey(key)
	if key == 'space' then
		self:saveCurrentPiece()
	elseif key == 'left' then
		self:tryMoveLeft()
	elseif key == 'right' then
		self:tryMoveRight()
	elseif key == 'down' then
		self:tryMoveDown()
	elseif key == 's' then
		self:tryRotateLeft()
	elseif key == 'd' then
		self:tryRotateRight()
	end
	if self.keysPressed[key] ~= nil then
		self.keysPressed[key].isReleased = false
	end
end

function Game:tryMoveLeft()
	local updatedPiece = utils.clone(self.currentPiece)
	updatedPiece.pos.x = updatedPiece.pos.x - 1
	if self.grid:canFit(updatedPiece) then
		self.currentPiece = updatedPiece
	end
end

function Game:tryMoveRight()
	local updatedPiece = utils.clone(self.currentPiece)
	updatedPiece.pos.x = updatedPiece.pos.x + 1
	if self.grid:canFit(updatedPiece) then
		self.currentPiece = updatedPiece
	end
end

function Game:tryMoveDown()
	local updatedPiece = utils.clone(self.currentPiece)
	updatedPiece.pos.y = updatedPiece.pos.y + 1
	if self.grid:canFit(updatedPiece) then
		self.currentPiece = updatedPiece
		self.timer = 0
	else
		local nbRemovedLines = self.grid:addPiece(self.currentPiece)
		self.lockTimer = nil
		self.hasLock = false
		self.hasSwap = false
		self:addScore(nbRemovedLines)
		self:spawnNextPiece()
	end
end

function Game:tryRotateLeft()
	local updatedPiece = utils.clone(self.currentPiece)
	self.rotatePiece(updatedPiece, 'left')
	if self.grid:canFit(updatedPiece) then
		self.currentPiece = updatedPiece
		if self.lockTimer then
			self.lockTimer = 0
		end
	else
		self:tryWallKick(updatedPiece)
	end
end

function Game:tryRotateRight()
	local updatedPiece = utils.clone(self.currentPiece)
	self.rotatePiece(updatedPiece, 'right')
	if self.grid:canFit(updatedPiece) then
		self.currentPiece = updatedPiece
		if self.lockTimer then
			self.lockTimer = 0
		end
	else
		self:tryWallKick(updatedPiece)
	end
end

function Game:tryWallKick(piece)
	local updatedPiece = piece
	updatedPiece.pos.x = updatedPiece.pos.x + 1
	if self.grid:canFit(updatedPiece) then
		self.currentPiece = updatedPiece
	else
		updatedPiece.pos.x = updatedPiece.pos.x - 2
		if self.grid:canFit(updatedPiece) then
			self.currentPiece = updatedPiece
		end
	end
	
end

function Game:keyreleased(key)
	if self.keysPressed[key] ~= nil then
		self.keysPressed[key].autoShiftTimer = 0
		self.keysPressed[key].shifting = false
		self.keysPressed[key].isReleased = true
	end
end

function Game:spawnNextPiece()
	-- select the following piece
	self.currentPiece = table.remove( self.nextPieces )
	-- create a new piece
	self:createNextRandomPiece()
	-- reset variables
	self.currentPiece.pos = {}
	self.currentPiece.pos.x = 4
	if self.currentPiece.color == 'cyan' then
		self.currentPiece.pos.y = 1
	else
		self.currentPiece.pos.y = 2
	end
	self.timer = 0
	self:initShifts()
	-- Game over
	self:checkGameOver()
end

function Game:createNextRandomPiece()
	local pieceKey = pieces[love.math.random( #pieces )]
	table.insert( self.nextPieces, 1, Tetrominos[pieceKey] )
end

function Game:initShifts()
	for key, value in pairs(keys) do
		self.keysPressed[value] = {
			autoShiftTimer = 0,
			shifting = false
		}
	end
end

function Game:saveCurrentPiece()
	if not self.hasSwap and not self.hasLock then
		if self.savedPiece then
			local restauredPiece = utils.clone(self.savedPiece)
			self.savedPiece = utils.clone(self.currentPiece)
			self.currentPiece = restauredPiece
		else
			self.savedPiece = utils.clone(self.currentPiece)
			self:spawnNextPiece()
		end
		self.hasSwap = true
		-- reset
		self.currentPiece.pos = {}
		self.currentPiece.pos.x = 4
		if self.currentPiece.color == 'cyan' then
			self.currentPiece.pos.y = 1
		else
			self.currentPiece.pos.y = 2
		end
		self.timer = 0
		self:initShifts()
	end
end

function Game.rotatePiece(piece, direction)
	-- The O don't rotate
	if piece.color ~= 'yellow' then
		local pieceWidth = #piece.blocks
		local pieceHeight = #piece.blocks[1]
		-- Initialize rotated piece
		local rotPiece = {}
		for i=1, pieceWidth do
			rotPiece[i] = {}
		end

		--- left rotation
		if direction == 'left' then
			local newX = 1
			local y = pieceHeight
			while( y >= 1) do
				local newY = 1
				local x = 1
				while(x <= pieceWidth) do
					rotPiece[newX][newY] = piece.blocks[x][y]
					x = x + 1
					newY = newY + 1
				end
				y = y - 1
				newX = newX + 1
			end
		-- right rotation
		elseif direction == 'right' then
			local newX = 1
			local y = 1
			while( y <= pieceHeight) do
				local newY = 1
				local x = pieceWidth
				while(x >= 1) do
					rotPiece[newX][newY] = piece.blocks[x][y]
					x = x - 1
					newY = newY + 1
				end
				y = y + 1
				newX = newX + 1
			end
		end
		--replace piece
		piece.blocks = rotPiece
	end
end

function Game:checkGameOver()
	local updatedPiece = utils.clone(self.currentPiece)
	if not self.grid:canFit(updatedPiece) then
		Gamestate.switch(Result, self.oldState)
	end
end

function Game:addGreyLines(nbLines)
	if nbLines ~= nil then
		if nbLines > 0 then
			for i=1, nbLines do
				self.grid:addGreyLine()
			end
		end
		-- if current piece is overlapping
		while not self.grid:canFit(self.currentPiece) do
			if self.currentPiece.pos.y <= 1 then
				break
			else
				self.currentPiece.pos.y = self.currentPiece.pos.y - 1
			end
		end
	end
end

function Game:addScore(nbRemovedLines)
	if nbRemovedLines < 5 then
		if nbRemovedLines ~= 0 then
			self.score = self.score + (1/self.fallFrequency)*scores[nbRemovedLines]
		end
	end
end

return Game
