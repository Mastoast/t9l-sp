tetrominos = {
	Z = {
		blocks = {
			{1, 1, 0},
			{0, 1, 1},
			{0, 0, 0}
		},
		color = 'red'
	},
	S = {
		blocks = {
			{0, 1, 1},
			{1, 1, 0},
			{0, 0, 0}
		},
		color = 'green'
	},
	J = {
		blocks = {
			{1, 0, 0},
			{1, 1, 1},
			{0, 0, 0},
		},
		color = 'blue'
	},
	L = {
		blocks = {
			{0, 0, 1},
			{1, 1, 1},
			{0, 0, 0},
		},
		color = 'orange'
	},
	I = {
		blocks = {
			{0, 0, 0, 0},
			{1, 1, 1, 1},
			{0, 0, 0, 0},
			{0, 0, 0, 0}
		},
		color = 'cyan'
	},
	O = {
		blocks = {
			{0, 1, 1, 0},
			{0, 1, 1, 0},
			{0, 0, 0, 0}
		},
		color = 'yellow'
	},
	T = {
		blocks = {
			{0, 1, 0},
			{1, 1, 1},
			{0, 0, 0}
		},
		color = 'pink'
	}
}

return tetrominos