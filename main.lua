local Gamestate = require 'modules.hump.gamestate'
local Menu = require 'src.states.menu'

menuState = Menu

utils = require 'src.utils'

screen = {
	x = love.graphics.getWidth(),
	y = love.graphics.getHeight()
}

colors = {
	red = {1, 0, 0},
	green = {0, 1, 0},
	blue = {0, 0, 1},
	orange = {1, 0.4, 0},
	cyan = {0, 1, 0.8},
	yellow = {1, 1, 0},
	pink = {1, 0, 0.7},
	grey = {0.7, 0.7, 0.7}
}

function love.load()
	math.randomseed(os.time())
	love.graphics.setBackgroundColor(0.3, 0.3, 0.3)
	local font = love.graphics.newFont('res/fonts/VCR_OSD_MONO_1.001.ttf')
	love.graphics.setFont(font)
	-- Menu
	Gamestate.switch(Menu)
end

function love.draw()
	Gamestate.draw()
end

function love.update(dt)
	Gamestate.update(dt)
end

function love.keypressed(key)
	if key == 'f5' then
		love.event.quit('restart')
	end
	if key == 'f3' then
		local mode = love.window.getFullscreen()
		love.window.setFullscreen(not mode)
	end
	--
	Gamestate.keypressed(key)
end

function love.keyreleased(key)
	Gamestate.keyreleased(key)
end
