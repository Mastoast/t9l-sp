function love.conf(t)
	t.version = "11.2"
	t.console = false
	t.window.title = 'Tetrominos Fury 2351'
	t.window.resizable = false
	t.window.vsync = true
	t.window.width = 1200
	t.window.height = 800
end
