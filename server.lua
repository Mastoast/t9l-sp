local socket = require 'socket'
local utils = require 'src.utils'
 
-- config socket
local udp = socket.udp()
local listening_port = 12345
udp:settimeout(0.1)
udp:setsockname('*', listening_port)

--
math.randomseed(os.time())

-- Parameters
local NB_MIN_PLAYER = 2
local restartTime
local restartDelay = 10.0
local difficultyIncreaseDelay = 17.0
local minFallFrequency = 0.05

-- Init variables
local newClient = {
    isReady = false,
    isAlive = true,
    lastDamage = nil,
    nbFrags = 0,
    name = nil,
    incomingLines = 0
}

local clients = {}

-- Game variables
local level
local fallFrequency

local onLobby = true
local serverIsOn = true
local gameStarted = "NOT_STARTED"
local nbPlayer = 0
local nbPlayerAlive

-- Functions definition
function startGame()
    level = 1
    fallFrequency = 0.8
    gameStarted = "STARTED"
    onLobby = false
    nbPlayerAlive = nbPlayer
    restartTime = socket.gettime() + restartDelay
    startingTime = socket.gettime()
    nextDifficultyUpdate = startingTime + difficultyIncreaseDelay
    print('Game started')
end

function generateID()
    local newId = ''
    for i=1, 3 do
        newId = newId..string.char(math.random(65,  90))
    end
    if clients[newId] ~= nil then
        newId = generateID()
    end
    lastId = newId
    return newId
end

function stopGame()
    -- reset client list
    clients = {}
    gameStarted = "NOT_STARTED"
    onLobby = true
    nbPlayer = 0
    print('Lobby started')
end

function allClientReady()
    if utils.tablelength(clients) < NB_MIN_PLAYER then
        return false
    end
    for client in pairs(clients) do
        if (not clients[client].isReady) then
            return false
        end
    end
    return true
end

function addRandomLine(client_id, nbLines)
    local listTarget = {}
    for key,value in pairs(clients) do
        if key ~= client_id and clients[key].isAlive == true then
            table.insert( listTarget, key )
        end
    end
    -- for local testing 
    --[[ 
    local testClient = clients[lastId]
    testClient.incomingLines = testClient.incomingLines + nbLines
    testClient.lastDamage = client_id
    --]]
    if #listTarget >= 1 then
        local id_Player = listTarget[math.random( #listTarget )]
        local randPlayer = clients[ id_Player ]
        randPlayer.incomingLines = randPlayer.incomingLines + nbLines
        randPlayer.lastDamage = client_id
    end
end

-- States
function serveLobby(udp)
    client_data, client_ip, client_port = udp:receivefrom()
    if client_data then
        client_id, message, params = client_data:match("^(%S*) (%S*) ?(.*)")
        -- Connections
        if (message == 'CONNECT') then
            local newId = generateID()
            clients[newId] = utils.clone(newClient)
            clients[newId].name = params
            clients[newId].ip = client_ip
            nbPlayer = nbPlayer + 1
            udp:sendto(string.format('CONNECTED %s', newId), client_ip, client_port)
            print(clients[newId].name, message, 'with id', newId)
        elseif (message == "DISCONNECT") then
            if clients[client_id] ~= nil then
                nbPlayer = nbPlayer - 1
                local client_name = clients[client_id].name
                print(client_name, message)
                clients[client_id] = nil
            end
        elseif (message == "IS_STARTED") then
            udp:sendto( gameStarted, client_ip, client_port )
        end
        -- Status
        if clients[client_id] then
            if (message == "READY") then
                clients[client_id].isReady = true
                if (allClientReady()) then
                    startGame()
                end
            elseif (message == "NOT_READY") then
                clients[client_id].isReady = false
            elseif (message == "PLAYERS") then
                local playerList = ''
                for key,player in pairs(clients) do
                    local playerRdy
                    if player.isReady then
                        playerRdy = '1'
                    else
                        playerRdy = '0'
                    end
                    playerList = playerList..player.name..'/'..playerRdy..';'
                end
                udp:sendto(string.format( "PLAYERS %s", playerList), client_ip, client_port )
            end
        end
    end
end

function serveGame(udp)
    -- handle difficulty
    handleDifficulty()
    --
    client_data, client_ip, client_port = udp:receivefrom()
    if client_data then
        client_id, message, params = client_data:match("^(%S*) (%S*) ?(.*)")
        local player = clients[client_id]
        local lastAttacker = clients[player.lastDamage] or nil
        if (message == "GAME_OVER") then
            player.isAlive = false
            nbPlayerAlive = nbPlayerAlive - 1
            -- increment frag for the last attacker
            if lastAttacker then
                lastAttacker.nbFrags = lastAttacker.nbFrags + 1
            end
            print(player.name, message)
        elseif (message == "WINNER") then
            print(player.name, message)
            stopGame()
        elseif (message == "IS_STARTED") then
            udp:sendto( gameStarted, client_ip, client_port )
        elseif (message == "GET_STATUS") then
            restartTime = socket.gettime() + restartDelay
            statusMessage = gameStatus(player)
            udp:sendto( statusMessage, client_ip, client_port )
        elseif (message == "LINES_COMPLETED") then
            print(player.name, message, params)
            addRandomLine(client_id, params)
        elseif (message == "NEW_LINES") then
            -- send number of grey lines, last attacker and self frags count
            if(player.incomingLines >= 1) then
                udp:sendto( string.format( "NB_LINES %i %s", player.incomingLines, lastAttacker.name), client_ip, client_port )
                player.incomingLines = 0
            else
                udp:sendto( "NB_LINES 0", client_ip, client_port )
            end
        end
    else
        -- Reset if no more signals
        if socket.gettime() > restartTime then
            print("No more signals, returning to lobby")
            stopGame()
        end
    end
end

-- Answers
function gameStatus(askingPlayer)
    local playersStatus = ''
    for key,player in pairs(clients) do
        local playerAlive
        if player.isAlive then
            playerAlive = '1'
        else
            playerAlive = '0'
        end
        playersStatus = playersStatus..playerAlive
    end
    restartTime = socket.gettime() + restartDelay
    return string.format( "STATUS %s %i %g", playersStatus, askingPlayer.nbFrags, fallFrequency)
end

function handleDifficulty()
    if socket.gettime() > nextDifficultyUpdate then
        if fallFrequency > 0.3 then
            fallFrequency = fallFrequency - 0.1
            nextDifficultyUpdate = nextDifficultyUpdate + difficultyIncreaseDelay
        else
			if (fallFrequency > minFallFrequency) then
                fallFrequency = fallFrequency - 0.01
                nextDifficultyUpdate = nextDifficultyUpdate + difficultyIncreaseDelay
			end
		end
    end
end

-- Begin
print(string.format('Server listening on port %i', listening_port))
while serverIsOn do
    if onLobby then
        serveLobby(udp)
    else
        serveGame(udp)
    end
end
