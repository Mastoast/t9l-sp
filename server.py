import socket
import random
import time
import re

class Game:
    def __init__(self, sock):
        # Parameters
        self.nbMinimalPlayer = 2
        self.restartTime = None
        self.timeout = 10.0
        self.difficultyIncreaseDelay = 17.0
        self.minFallFrequency = 0.05
        # Variables
        self.clients = {}
        self.serverIsOn = True
        self.gameStarted = "NOT_STARTED"
        self.nbPlayer = 0
        # Socket
        self.sock = sock
        # State
        self.state = ServeLobby(self)

    # main loop
    def start_server(self):
        while game.serverIsOn:
            self.state.handle(self.sock)

    # Functions definition
    def startGame(self):
        self.fallFrequency = 0.8
        self.gameStarted = "STARTED"
        self.state = ServeGame(self)
        self.nbPlayerAlive = self.nbPlayer
        self.restartTime = time.time() + self.timeout
        self.startingTime = time.time()
        self.nextDifficultyUpdate = self.startingTime + self.difficultyIncreaseDelay
        print('Game started')
    
    def stopGame(self):
        # reset client list
        self.clients = {}
        self.gameStarted = "NOT_STARTED"
        self.state = ServeLobby(self)
        self.nbPlayer = 0
        print('Lobby started')
    
    def generateID(self):
        newID = ''
        for i in range(3):
            newID = newID + chr(random.randint(65, 90))
        if newID in self.clients:
            newID = generateID(game)
        return newID

    def allClientReady(self):
        if len(self.clients) < self.nbMinimalPlayer:
            return False
        for client in self.clients:
            if not self.clients[client].isReady:
                return False
        return True

    def addRandomLine(self, clientID, nbLines):
        targetList = []
        for id, client in self.clients.items():
            # get all potential target
            if id != clientID and client.isAlive:
                targetList.append(id)
        if len(targetList) >= 1:
            targetID = targetList[random.randint(0, len(targetList) - 1)]
            self.clients[targetID].incomingLines += nbLines
            self.clients[targetID].lastDamage = clientID
        else:
            self.clients[clientID].incomingLines += nbLines
            self.clients[clientID].lastDamage = clientID
    
    # Answers
    def gameStatus(self, askingPlayer):
        playersStatus = ''
        for key, player in self.clients.items():
            if player.isAlive:
                playerAlive = '1'
            else:
                playerAlive = '0'
            playersStatus += playerAlive
        self.restartTime = time.time() + self.timeout
        return "STATUS {} {} {}".format(playersStatus, askingPlayer.nbFrags, self.fallFrequency)

    def updateDifficulty(self):
        if time.time() > self.nextDifficultyUpdate:
            if self.fallFrequency > 0.3:
                self.fallFrequency -= 0.1
            else:
                if(self.fallFrequency > self.minFallFrequency):
                    self.fallFrequency -= 0.01
            self.nextDifficultyUpdate += self.difficultyIncreaseDelay

class Client:
    def __init__(self, name, ip):
        self.isReady = False
        self.isAlive = True
        self.lastDamage = None
        self.nbFrags = 0
        self.name = name
        self.incomingLines = 0

# States
class ServeLobby:
    def __init__(self, game):
        self.game = game
        # Pattern
        self.pattern = "^(\w+) (\w+) ?(.*)"

    def handle(self, sock):
        try:
            frame = sock.recvfrom(1024)
            clientData = frame[0].decode()
            clientIP, clientPort = frame[1]
        except socket.timeout:
            clientData = None
        if clientData:
            clientID, message, params = re.match(self.pattern, clientData).groups()
            # Connections
            if message == "CONNECT":
                newID = self.game.generateID()
                self.game.clients[newID] = Client(params, clientIP)
                self.game.nbPlayer += 1
                sock.sendto("CONNECTED {}".format(newID).encode(), (clientIP, clientPort))
                print(self.game.clients[newID].name, message, 'with id', newID)
            elif message == "DISCONNECT":
                oldClient = self.game.clients.pop(clientID, None)
                if oldClient:
                    self.game.nbPlayer -= 1
                    print(oldClient.name, message)
            elif message == "IS_STARTED":
                sock.sendto(self.game.gameStarted.encode(), (clientIP, clientPort))
            # Status
            if clientID in self.game.clients:
                if message == "READY":
                    self.game.clients[clientID].isReady = True
                    if (self.game.allClientReady()):
                        self.game.startGame()
                elif message == "NOT_READY":
                    self.game.clients[clientID].isReady = False
                elif message == "PLAYERS":
                    playerList = ''
                    for key, player in self.game.clients.items():
                        if player.isReady:
                            playerRdy = '1'
                        else:
                            playerRdy = '0'
                        playerList += "{}/{};".format(player.name, playerRdy)
                    frame = "PLAYERS {}".format(playerList)
                    sock.sendto(frame.encode(), (clientIP, clientPort))

class ServeGame:
    def __init__(self, game):
        self.game = game
        # Pattern
        self.pattern = "^(\w+) (\w+) ?(.*)"

    def handle(self, sock):
        # Update game speed
        self.game.updateDifficulty()
        
        # Handle data
        try:
            frame = sock.recvfrom(1024)
            clientData = frame[0].decode()
            clientIP, clientPort = frame[1]
        except socket.timeout:
            clientData = None
        if clientData:
            clientID, message, params = re.match(self.pattern, clientData).groups()
            if message == "CONNECT" :
                # server is busy
                sock.sendto("BUSY".encode(), (clientIP, clientPort))
                return
            elif message == "DISCONNECT":
                # kill player if disconnected
                message == "GAME_OVER"
            player = self.game.clients[clientID]
            if player.lastDamage:
                lastAttacker = self.game.clients[player.lastDamage]
            else:
                lastAttacker = None
            if message == "GAME_OVER":
                player.isAlive = False
                self.game.nbPlayerAlive -= 1
                # add frag for last attacker
                if lastAttacker:
                    lastAttacker.nbFrags += 1
                print(player.name, message)
            elif message == "WINNER":
                print(player.name, message)
                self.game.stopGame()
            elif message == "IS_STARTED":
                sock.sendto(self.game.gameStarted.encode(), (clientIP, clientPort))
            elif message == "GET_STATUS":
                self.game.restartTime = time.time() + self.game.timeout
                statusMessage = self.game.gameStatus(player)
                sock.sendto(statusMessage.encode(), (clientIP, clientPort))
            elif message == "LINES_COMPLETED":
                print(player.name, message, params)
                self.game.addRandomLine(clientID, int(params))
            elif message == "NEW_LINES":
                # Send number of grey lines, last attacker and self frags count
                if player.incomingLines >= 1:
                    frame = "NB_LINES {} {}".format(player.incomingLines, lastAttacker.name)
                    sock.sendto(frame.encode(), (clientIP, clientPort))
                    player.incomingLines = 0
                else:
                    sock.sendto("NB_LINES 0".encode(), (clientIP, clientPort))
        else:
            # reset if no more signals
            if time.time() > self.game.restartTime:
                print("No more signals, returning to lobby")
                self.game.stopGame()

# Main
if __name__ == "__main__":
    # config socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    listening_port = 12345
    sock.settimeout(0.1)
    sock.bind(('',listening_port))

    # Seed the random
    random.seed(time.time())

    print("Server listening on port {}".format(listening_port))

    # Starting the game
    game = Game(sock)
    game.start_server()
