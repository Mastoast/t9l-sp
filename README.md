# Tetrominos Fury 2351

Tetrominos Fury 2351 is a protoype of a multiplayer tetris game that use udp sockets for communication.
It uses Lua for running the server and Löve for clients

## Commands

### General
R to restart the game
M to switch fullscreen on/off

### In lobby
V to paste the clipboard content in the ip field
Enter key to try connecting to the specified IP and port
ENter key to set ready/not ready
### In game
Left / Right arrow keys to move the piece
Down arrow key to move the piece down
S and D to rotate the piece
Space to swap the current piece with the saved one


## Credits

### Testers
* Deldrimo
* Effarek
* Youpi
* Volothamp
* Coconul
* Altaxl
* Not

### Fonts
VCR OSD Mono

### Modules
hump https://github.com/vrld/hump
