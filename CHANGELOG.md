## 0.6.5-beta (14-05-2019)
* added background and title in main menu

## 0.6.4-beta (30-03-2019)

### Features
* added player list in multiplayer games
* specified the field focused in the connection screen

### Bug Fixes
* playing with multiple client from the same ip is now possible
* grey lines don't spawns only when locking a piece anymore

## 0.6.3-beta (23-03-2019)

### Features
* added a main menu
    * added singleplayer direct start
    * added quit button
    * added credits menu (empty for now)
    * added game version in game

### Modifications
* updated the lock system
    * set the lock time at 0.5 seconds
    * lock time reset each time the piece is falling again
    * lock time at each piece rotation

## 0.6.2-beta (19-03-2019)

### Modifications

* changed game files architecture to improve extensibility
* refactored objects and game states
* **BR mode:** added server-controlled fall speed


## 0.6.1-beta (18-03-2019)

### Features

* **menu:** added restoration of the ip address, port and player name when returning to the connecting screen
* **menu:** added a limit of 20 characters on text fields (name, ip and port)
* **menu:** added the possibility of using the Numpad's enter key as the return key
* **game:** added frags counter in game

### Modifications

* **menu:** changed fullscreen key from M to F3
* **menu:** changed restart key from R to F5
* **menu:** changed paste command to from V to CTRL + V 

### Bug fixes

* **server:** fixed server crash when a client disconnect
* **server:** fixed server crash when loosing without any attacker
* **server:** fixed server crash when sending a line while alone in the game
* **win screen:** no longer display the name of the last attacker for the winner
* **win screen:** changed the good number of frag on the win screen


# 0.6.0-beta Player name update (17-03-2019)

### Features
* **client:** added player name in the connect screen
* **lobby:** added players list and their status in lobby
* **ending screen:** added the number of frags and the name of the player that eliminated you
* **server:** added an automatic restart when receiving no more signals in a started game

# 0.5.2-beta (17-03-2019)

### Features
* **server:** added control of multiple Connect and Disconnect signals from the same client
* **graphics:** added shape of incoming pieces instead of their color name

### Bug Fixes
* **solo:** solo games no more display multiplayer parameters
* **grid:** swapping a piece when it is about to lock no longer lock it at the top of the grid
* **game name:** the game name Changed from Tetrominos Fury 2361 to Tetrominos Fury 2351
